import {Min} from 'class-validator';
import {CreateCatInput} from '../cat.schema';

export class CreateCatDto extends CreateCatInput {
  @Min(1)
  age: number;
}
