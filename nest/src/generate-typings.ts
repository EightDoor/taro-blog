import {GraphQLDefinitionsFactory} from '@nestjs/graphql';
import {join} from 'path';

const definitionsFactory = new GraphQLDefinitionsFactory();
definitionsFactory.generate({
  typePaths: ['./src/business/**/*.graphql'],
  path: join(process.cwd(), 'src/business/**/**.schema.ts'),
  outputAs: 'class',
});
