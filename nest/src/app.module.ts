import {Module} from '@nestjs/common';

// 注册graphQl
import {GraphQLModule} from '@nestjs/graphql';

import {CatsModule} from './business/cats/cats.module';

@Module({
  imports: [
    CatsModule,
    GraphQLModule.forRoot({
      typePaths: ['./src/business/**/*.graphql'],
      installSubscriptionHandlers: true,
    }),
  ],
})
export class AppModule {
}
