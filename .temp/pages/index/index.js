import Nerv from "nervjs";
import Taro from "@tarojs/taro-h5";
import { View } from '@tarojs/components';
import { AtTabBar } from 'taro-ui';
import './index.less';
import Home from '../tabs/home/home';
import My from '../tabs/my/my';

class Index extends Taro.Component {
  render() {
    const [current, setCurrent] = Taro.useState(0);
    const [tabList, setTabList] = Taro.useState([{
      title: '首页',
      image: 'https://img20.360buyimg.com/jdphoto/s72x72_jfs/t15151/308/1012305375/2300/536ee6ef/5a411466N040a074b.png'
    },
    // {
    //   title: '其他',
    //   image:
    //     'https://img20.360buyimg.com/jdphoto/s72x72_jfs/t15151/308/1012305375/2300/536ee6ef/5a411466N040a074b.png'
    // },
    {
      title: '关于',
      image: 'https://img20.360buyimg.com/jdphoto/s72x72_jfs/t15151/308/1012305375/2300/536ee6ef/5a411466N040a074b.png'
    }]);
    const handleClick = value => {
      setCurrent(value);
    };
    return <View className="container">
      <AtTabBar tabList={tabList} onClick={handleClick} current={current} fixed />
      {current === 0 ? <Home /> : null}
      {current === 1 ? <My /> : null}
    </View>;
  }

  componentDidMount() {
    super.componentDidMount && super.componentDidMount();
  }

  componentDidShow() {
    super.componentDidShow && super.componentDidShow();
  }

  componentDidHide() {
    super.componentDidHide && super.componentDidHide();
  }

}

Index.config = {
  navigationBarTitleText: '首页'
};
export default Index;