import Nerv from "nervjs";
import Taro from "@tarojs/taro-h5";
import { View, Text, ScrollView } from '@tarojs/components';
import './my.less';
import { ClTimeline } from 'mp-colorui';

class My extends Taro.Component {
  render() {
    const value = [{
      title: '初始化哦😊',
      bgColor: 'red',
      content: ['技术栈: ', '时间: 2019年10月4号 ﾍ(･_|', '前端->taro taro-ui mp-colorui typescript graphql hooks', '后端->nestjs mongodb graphql']
    }];
    const onScrollToUpper = e => {
      console.log(e.detail);
    };
    const onScrollToLower = e => {
      console.log(e.detail);
    };
    const onScroll = e => {
      // console.log(e.detail)
    };
    const scrollTop = 0;
    const Threshold = 20;
    const [items, setItems] = Taro.useState(value);
    return <ScrollView className="scrollview" scrollY scrollWithAnimation scrollTop={scrollTop} lowerThreshold={Threshold} upperThreshold={Threshold} onScrollToLower={onScrollToLower} onScrollToUpper={onScrollToUpper} // 使用箭头函数的时候 可以这样写 `onScrollToUpper={this.onScrollToUpper}`
    onScroll={onScroll}>
      <View className="taro-padding">
        <Text className="title">更新记录</Text>
        <ClTimeline times={items} />
      </View>
    </ScrollView>;
  }

}

export default My;