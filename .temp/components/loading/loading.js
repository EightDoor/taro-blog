import Nerv from "nervjs";
import Taro from "@tarojs/taro-h5";
import './loading.less';
import { AtActivityIndicator } from 'taro-ui';
import { LoadingContext } from '../context';

class Loading extends Taro.Component {
  render() {
    const text = Taro.useContext(LoadingContext);
    return <AtActivityIndicator content={text.content} color={text.color} mode="center"></AtActivityIndicator>;
  }

}

export default Loading;