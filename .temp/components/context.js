import Taro from "@tarojs/taro-h5";
const LoadingState = {
  content: '加载中...',
  color: 'red'
};
const LoadingContext = Taro.createContext({});
export { LoadingContext, LoadingState };