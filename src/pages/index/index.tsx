import Taro, { useState } from '@tarojs/taro'
import { View } from '@tarojs/components'
import { AtTabBar } from 'taro-ui'
import './index.less'
import { TabList } from 'src/interface/tabs'
import Home from '../tabs/home/home'
import My from '../tabs/my/my'

const Index = () => {
  const [current, setCurrent] = useState<number>(0)
  const [tabList, setTabList] = useState<TabList[]>([
    {
      title: '首页',
      iconType: 'home'
    },
    // {
    //   title: '其他',
    //   image:
    //     'https://img20.360buyimg.com/jdphoto/s72x72_jfs/t15151/308/1012305375/2300/536ee6ef/5a411466N040a074b.png'
    // },
    {
      title: '关于',
      iconType: 'alert-circle'
    }
  ])
  const handleClick = (value: number) => {
    setCurrent(value)
  }
  return (
    <View className="container">
      <AtTabBar
        tabList={tabList}
        onClick={handleClick}
        current={current}
        fixed
      />
      {current === 0 ? <Home /> : null}
      {current === 1 ? <My /> : null}
    </View>
  )
}

Index.config = {
  navigationBarTitleText: '首页'
}
export default Index
