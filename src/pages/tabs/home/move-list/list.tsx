import Taro, { useState, useMemo } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import { AtTabs, AtTabsPane } from 'taro-ui'
import ScrollList from '../../../../components/scroll-list/scroll'
import './list.less'
import IsHit from './is-hit/is-hit'

const MoveList = () => {
  const [current, setCurrent] = useState<number>(0)
  const [tabList, setTabList] = useState<{ title }[]>([
    {
      title: '正在热映'
    },
    {
      title: '即将上映'
    }
  ])
  const handleClick = (value: number) => {
    setCurrent(value)
  }
  const ChildHeight = 44
  return (
    <View>
      <AtTabs current={current} scroll tabList={tabList} onClick={handleClick}>
        <AtTabsPane current={current} index={0}>
          <ScrollList height={ChildHeight}>
            <IsHit />
          </ScrollList>
        </AtTabsPane>
        <AtTabsPane current={current} index={1}>
          <View style="font-size:18px;text-align:center;height:100px;">
            标签页二的内容
          </View>
        </AtTabsPane>
      </AtTabs>
    </View>
  )
}
export default MoveList
