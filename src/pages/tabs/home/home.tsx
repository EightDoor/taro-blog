import Taro, { useState } from '@tarojs/taro'
import { View, ScrollView, Swiper, SwiperItem, Image } from '@tarojs/components'
import './home.less'
import { LoadingContext, LoadingState } from '../../../components/context'
import Loading from '../../../components/loading/loading'
import { AtNoticebar, AtTabs, AtTabsPane, AtTabBar } from 'taro-ui'
import VerticalTabs from './verticalTabs'

const Home = () => {
  const listItem = [
    'https://mp-yys-1255362963.cos.ap-chengdu.myqcloud.com/mp-bgcolor/swiper2.jpg',
    'https://mp-yys-1255362963.cos.ap-chengdu.myqcloud.com/mp-bgcolor/swiper1.jpg'
  ]
  const scrollTitle: string = '初始化建立->2019年10月4号'
  const [loading, setLoading] = useState<boolean>(false)
  const onScrollToUpper = e => {
    console.log(e.detail)
  }
  const onScrollToLower = e => {
    console.log(e.detail)
  }
  const onScroll = e => {
    // console.log(e.detail)
  }
  const ListImage = () => {
    return listItem.map((item: string) => {
      return (
        <SwiperItem key={item}>
          <Image className="images" src={item} />
        </SwiperItem>
      )
    })
  }
  const scrollTop = 0
  const Threshold = 20
  return (
    <View>
      {loading ? (
        <View>
          <LoadingContext.Provider value={LoadingState}>
            <Loading />
          </LoadingContext.Provider>
        </View>
      ) : null}
      <ScrollView
        className="scrollview"
        scrollY
        scrollWithAnimation
        scrollTop={scrollTop}
        lowerThreshold={Threshold}
        upperThreshold={Threshold}
        onScrollToLower={onScrollToLower}
        onScrollToUpper={onScrollToUpper} // 使用箭头函数的时候 可以这样写 `onScrollToUpper={this.onScrollToUpper}`
        onScroll={onScroll}
      >
        <View>
          <AtNoticebar marquee>{scrollTitle}</AtNoticebar>
          <Swiper
            className="test-h"
            indicatorColor="#999"
            indicatorActiveColor="#333"
            vertical
            circular
            indicatorDots
            autoplay
          >
            {ListImage}
          </Swiper>
          <VerticalTabs></VerticalTabs>
        </View>
      </ScrollView>
    </View>
  )
}

export default Home
