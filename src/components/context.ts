import { createContext } from '@tarojs/taro'

/**
 * 加载动画
 */
import { LoadingInter } from 'src/interface/tabs'
const LoadingState: LoadingInter = {
  content: '加载中...',
  color: 'red'
}
const LoadingContext = createContext<LoadingInter>({})

/**
 * 滚动列表组件
 */
const ScrollListProps = createContext({})
export { LoadingContext, LoadingState, ScrollListProps }
