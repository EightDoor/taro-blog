import Taro, { useContext } from '@tarojs/taro'
import './loading.less'
import { AtActivityIndicator } from 'taro-ui'
import { LoadingContext } from '../context'

const Loading = () => {
  const text = useContext(LoadingContext)
  return (
    <AtActivityIndicator
      content={text.content}
      color={text.color}
      mode="center"
    ></AtActivityIndicator>
  )
}

export default Loading
