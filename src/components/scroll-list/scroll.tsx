import Taro, { useContext, useEffect } from '@tarojs/taro'
import { View, Text, ScrollView } from '@tarojs/components'
import propTypes from 'prop-types'

const ScrollList = ({ children, height }) => {
  const scrollTop = 0
  const Threshold = 20
  useEffect(() => {
    // console.log(name)
  })
  const onScrollToUpper = e => {
    console.log(e.detail)
    return e.detail
  }
  const onScrollToLower = e => {
    console.log(e.detail)
    return e.detail
  }
  const onScroll = e => {
    // console.log(e.detail)
  }
  return (
    <ScrollView
      style={{ height: `calc(100vh - ${height}px)` }}
      scrollY
      scrollWithAnimation
      scrollTop={scrollTop}
      lowerThreshold={Threshold}
      upperThreshold={Threshold}
      onScrollToLower={onScrollToLower}
      onScrollToUpper={onScrollToUpper} // 使用箭头函数的时候 可以这样写 `onScrollToUpper={this.onScrollToUpper}`
      onScroll={onScroll}
    >
      <View>{children}</View>
    </ScrollView>
  )
}

ScrollList.propTypes = {
  children: propTypes.element.isRequired,
  height: propTypes.number
}
export default ScrollList
