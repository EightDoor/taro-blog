/**
 * 首页底部列表
 */
export interface TabList {
  title: string
  image?: string
  text?: string
  dot?: boolean
  selectedImage?: string
  iconType?: string
}
/**
 * Loading
 */

export interface LoadingInter {
  content?: string
  color?: string
}

/**
 *首页菜单
 *
 * @export
 * @interface BottomMenu
 */
export interface BottomMenu {
  image: string
  value: string
}
